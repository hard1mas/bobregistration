﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class _Default : System.Web.UI.Page
{
    String filePath;
    bool valid = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void validate()
    {
        valid = true;
        valid = !String.IsNullOrEmpty(firstName.Text);
        if(valid)
        {
            valid = !String.IsNullOrEmpty(emailAddress.Text);
        }
        if (valid)
        {
            //very simple check whether the email address contains @ or not.
            valid = emailAddress.Text.Contains('@');
        }
    }
    protected String cleanPhoneNumber()
    {
        String[] numbers = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        String cleanPhone = phoneNumber.Text;
        for (int i = 0; i < cleanPhone.Length; i++)
        {
            if (!numbers.Contains(cleanPhone[i].ToString()))
            {
                cleanPhone = cleanPhone.Replace(cleanPhone[i].ToString(), " ");
            }
        }
        cleanPhone = cleanPhone.Replace(" ", "");
        return cleanPhone;
    }
    protected void writeValues(object sender, EventArgs e)
    {
        filePath = Server.MapPath("~/App_Data/registrations.txt");
        try
        {
            validate();
            if (valid)
            {
                String cleanPhone = cleanPhoneNumber();
                String userInput = firstName.Text + " " + lastName.Text + ";" + emailAddress.Text + "," + cleanPhone + ";" + Major.Text;
                //output format looks like this: Mason Hardy;hard1mas@live.bemidjistate.edu,2182444076;Computer Science
                String[] fileContents = File.ReadAllLines(filePath);
                String[] toWrite = new String[fileContents.Length + 1];
                for (int i = 0; i < fileContents.Length; i++)
                {
                    toWrite[i] = fileContents[i];
                }
                toWrite[toWrite.Length - 1] = userInput;
                File.WriteAllLines(filePath, toWrite);
            }
        }
        catch (Exception ex)
        {
            Page.Title = ex.Message;
        }
        Response.Redirect("Default.aspx");
    }
}
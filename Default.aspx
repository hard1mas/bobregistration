﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" Runat="Server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="BodyContent" Runat="Server" ClientIDMode="Static">
    <div id="bodyHeading">
        Computer Science Club Registration
    </div>
    <asp:Panel ID="registrationPanel" ClientIDMode="Static" runat="server">
        First Name: <asp:TextBox ID="firstName" ClientIDMode="Static" runat="server" TextMode="SingleLine"/>
        Last Name: <asp:TextBox ID="lastName" ClientIDMode="Static" runat="server" TextMode="SingleLine"/>
        Phone Number: <asp:TextBox ID="phoneNumber" ClientIDMode="Static" TextMode="SingleLine" runat="server" />
        <br />
        Email Address: <asp:TextBox ID="emailAddress" ClientIDMode="Static" TextMode="SingleLine" runat="server" />
        Major: <asp:TextBox ID="Major" ClientIDMode="Static" TextMode="SingleLine" runat="server" />
        <br />
        <asp:Button OnClick="writeValues" runat="server" ClientIDMode="Static" ID="submitButton" Text="Submit Values"/>
        <br />
        <asp:Label ID="ErrorReport" ClientIDMode="Static" runat="server" />
    </asp:Panel>
</asp:Content>

